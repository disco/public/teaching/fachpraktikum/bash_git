#! /bin/bash
count=3
for file in allTheData.csv numberOfLines.txt firstLines.txt latestData.txt; do
	checksum="$(md5sum $file | cut -f1 -d" ")"; # get checksum of the student's file
	verifysum="$(grep $file md5sums.txt | cut -f1 -d" ")"; # read appropriate checksum from our file
	if [ "$checksum" == "$verifysum" ]; then
		echo "Exercise 2.$count solved correctly";
	else
		echo "Exercise 2.$count not solved correctly";
		exit 1
	fi;
	count=$(($count+1));
done
