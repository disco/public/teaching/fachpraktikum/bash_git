#!/bin/bash
echo "Verifying"
md5sum -c --quiet --status exercise13.md5 2> /dev/null
if [ $? -eq 0 ]; then
  echo "Exercise 1.2 solved correctly" 
  echo "Exercise 1.3 solved correctly" 
else
  md5sum -c --quiet --status exercise12.md5 2> /dev/null
  if [ $? -eq 0 ]; then
    echo "Exercise 1.2 solved correctly" 
    echo "Exercise 1.3 not solved correctly"
  else
    echo "Exercise 1.2 not solved correctly"
  fi
  exit 1
fi


md5sum -c --quiet --status exercise14.md5 2> /dev/null
if [ $? -eq 0 ]; then
  echo "Exercise 1.4 solved correctly" 
else
  echo "Exercise 1.4 not solved correctly"
  exit 1
fi

cd csv-Files
EXERCISE15=`cat csvFilesList.txt | sort | md5sum |  cut -d " " -f1` 2> /dev/null
# with or without './'
if [ "$EXERCISE15" = "35479dbcbe6a47ec5d49cf20d238b835" ] || [ $EXERCISE15 = "f7622a32ca1e5205ff186438f4b82bd8" ]; then
  echo "Exercise 1.5 solved correctly" 
else
  echo "Exercise 1.5 not solved correctly"
  exit 1
fi
 
 
EXERCISE16=`cat allCSVFilesContent.txt | sort | md5sum |  cut -d " " -f1` 2> /dev/null
if [ $EXERCISE16 = "3f46e80310a5c8cca116780f007bcbf6" ]; then
  echo "Exercise 1.6 solved correctly" 
else
  echo "Exercise 1.6 not solved correctly"
  exit 1
fi
 
cd ../src/core
 
md5sum -c --quiet --status exercise18.md5 2> /dev/null
if [ $? -eq 0 ]; then
  echo "Exercise 1.8 solved correctly" 
else
  echo "Exercise 1.8 not solved correctly"
  exit 1
fi

FILES10="BloomFilter.java GetBlocksMessage.java NetworkParameters.java PrunedException.java TransactionOutPoint.java VersionAck.java"
for file in $FILES10; do
  if [ -f $file ];then
    echo "Exercise 1.10 not solved correctly"
    exit 1
  fi
done
echo "Exercise 1.10 solved correctly"

cd ../utils
FILES12="ContextPropagatingThreadFactory.java ExponentialBackoff.java MonetaryFormat.java Threading.java BlockFileLoader.java  BtcFixedFormat.java DaemonThreadFactory.java package-info.java BriefLogFormatter.java  BtcFormat.java  ExchangeRate.java  ListenerRegistration.java Files\ With\ Spaces\ Can\ Be\ Annoying.java"


for file in $FILES12; do
  if [ -f $file ];then
    echo "Exercise 1.12 not solved correctly"
    exit 1
  fi
done

FILES122="BaseTaggableObject.java BtcAutoFormat.java Fiat.java TaggableObject.java VersionTally.java"
for file in $FILES122; do
  if [ ! -f $file ];then
    echo "Exercise 1.12 not solved correctly"
    exit 1
  fi
done
echo "Exercise 1.12 solved correctly"
echo "Continue with Exercise 2"